package com.cpl_cursos.java.pruebaAjax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class pruebaAjaxApplication {
    public static void main(String[] args) {
        SpringApplication.run(pruebaAjaxApplication.class, args);
    }
}
