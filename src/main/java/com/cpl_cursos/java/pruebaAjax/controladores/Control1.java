package com.cpl_cursos.java.pruebaAjax.controladores;

import com.cpl_cursos.java.pruebaAjax.modelos.Voto;
import com.cpl_cursos.java.pruebaAjax.servicios.VotoSrvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.ZonedDateTime;

@Controller
public class Control1 {

    @Autowired
    private VotoSrvc votosrvc;

    @GetMapping("/inicio")
    public String inicio(){
        return "inicio";
    }

    @GetMapping(value = "/otroMsg")
    public String moreMessage(Model m) {
        m.addAttribute("msg", "La hora del servidor es " + ZonedDateTime.now());
        return "bloques/partesAjax :: time-info ";
    }

    @GetMapping("/votar")
    public String votar(){
        return "votos";
    }

    @PostMapping("/cuentavotos")
    public String cuentaVotos(@RequestParam(name="valor") int voto, Model m){
        Voto v = new Voto();
        v.setVoto(voto);
        votosrvc.grabar(v);
        m.addAttribute("voto", voto);
        m.addAttribute("suma", votosrvc.sumaVotos());
        m.addAttribute("cuenta", votosrvc.cuentaVotos());
        return "bloques/partesAjax :: votos ";
    }

}
