package com.cpl_cursos.java.pruebaAjax.servicios;

import com.cpl_cursos.java.pruebaAjax.modelos.Voto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cpl_cursos.java.pruebaAjax.repos.ifxVotoRepo;

@Service
public class VotoSrvc {

    @Autowired
    private ifxVotoRepo votoRepo;

    public void grabar(Voto v){
        votoRepo.save(v);
    }

    public Integer sumaVotos() {
        return votoRepo.sumaVotos();
    }

    public Integer cuentaVotos(){
        return votoRepo.cuentaVotos();
    }
}
