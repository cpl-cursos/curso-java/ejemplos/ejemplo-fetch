package com.cpl_cursos.java.pruebaAjax.repos;

import com.cpl_cursos.java.pruebaAjax.modelos.Voto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ifxVotoRepo extends JpaRepository<Voto, Long> {

    @Query("SELECT SUM(v.voto) FROM Voto v")
    public Integer sumaVotos();

    @Query("SELECT COUNT(v) FROM Voto v")
    public Integer cuentaVotos();
}
